/* Inpired by Jee Dribbble Shot ( http://dribbble.com/shots/770815-Login ) */ 
/* coded by alireza attari ( @alireza_attari ) */ 

$(document).ready(function() {

	$('.signin').click(function() {
		$('.signin-modal').modal();
	});
    $('.signup').click(function() {
        $('.signup-modal').modal();
    });
	$('.cart').click(function() {
		$('.cart-modal').modal();
	});
    $('.categories').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('cid');
        $.ajax({
			url: '/action.php',
			method: 'post',
			data: { category_id:id },
			success: function (data) {
				$('#product_load').fadeIn(500).html(data);
            }
		})
    });
    $('#search').click(function (e) {
    	e.preventDefault();
		var nx = $('#input-search').val();
		if(nx != ""){
            $.ajax({
                url: '/action.php',
                method: 'post',
                data: { query:nx },
                success: function (data) {
                    $('#product_load').fadeIn(500).html(data);
                }
            })
		}
    })
    $('#signup-button').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/signup.php',
            method: 'post',
            data: $("form.signup-form").serialize(),
            success: function (data) {
                $('.messages').fadeIn(500).html(data);
            }
        })
    })
    $('#signin-button').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/signin.php',
            method: 'post',
            data: $("form.signin-form").serialize(),
            success: function (data) {
                if(data == 'ssi'){
                    window.location.href = '/profile.php';
                }
            }
        })
    })
    $("body").delegate(".add-to-cart", "click", function(e){
        e.preventDefault();
        var pid = $(this).attr('pid');
        $.ajax({
            url: '/action.php',
            method: 'post',
            data: {pid: pid},
            success: function (data) {
                alert(data);
            }
        })
    })
    $('.cart').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: 'action.php',
            method: 'post',
            data: { get_shop_cart:1 },
            success: function (data) {
                $('.product-list').fadeIn(500).html(data);
            }
        })
    })
    $("body").delegate(".qty", "keyup", function(){
        var pid = $(this).attr('pid');
        var qty = $('#qty-' + pid).val();
        var price = $('#price-' + pid).val();
        var total = qty*price;
        $('#total-' + pid).val(total);
    })
    $("body").delegate(".delete", "click", function(e){
        e.preventDefault();
        var delp = $(this).attr('delete_id');
        $.ajax({
            url: 'action.php',
            method: 'post',
            data: {
                removeFromCart:1,
                delp: delp
            },
            success: function (data) {
                $('.cart-message').fadeIn(500).html(data);
            }
        })
    })
    $("body").delegate(".update", "click", function(e){
        e.preventDefault();
        var upid = $(this).attr('update_id');
        var qty = $('#qty-'+upid).val();
        var total = $('#total-' + upid).val();
        alert(qty);
    })
});