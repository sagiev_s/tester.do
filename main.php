<!DOCTYPE html>
<html>
<head>
	<title>Google AdWords</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<div class="row">
		<h1>Google AdWords</h1>
		<p>Каждую фразу или шаблон нужно разделять через новую строку. Для подстановки нужно использовать {keyword}</p>
		<div class="col-md-6">
			<label for="template">Ключевые фразы:</label><br>
			<textarea name="keyword" id="keyword" cols="6" rows="6" class="form-control"></textarea>
		</div>
		<div class="col-md-6">
			<label for="template">Шаблоны заголовков:</label><br>
			<textarea name="title" id="title" cols="6"  rows="6" class="form-control"></textarea>
		</div>
		<div class="col-md-6"></div>
		<div class="col-md-6">
			<label for="template">Шаблоны текстов:</label><br>
			<textarea name="body" id="body" cols="6" rows="6" class="form-control"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="button" class="btn btn-info" style="margin-top: 10px">Старт</button>
			<div class="show"></div>
		</div>
	</div>
</div>
<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
	$(".btn").click(function() {
		$('.table').show();
		var data = { keyword: $("#keyword").val(),
								 title: $("#title").val(),
								 body: $("#body").val(), }
		$.ajax({
			method: 'POST',
			data: data,
			url: 'back.php',
			success: function(data) {
				$(".show").html(data);
			},
		});

	});
</script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>