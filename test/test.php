<!DOCTYPE html>
<html>
<head>
	<title>Test</title>
	<meta charset="utf-8">
	<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
</head>
<body>
  <h1>ajax sample</h1>
  your name: <input type="text" id="name">
  <div class="hello"></div>
  <button class="btn">Send</button> 
	<script>
		$(".btn").click(function() {
			var params = { text: $("#name").val(), }
			$.post("ajax.php", params, function(data) {
				$(".hello").html(data);
			})
		});
	</script>
</body>
</html>