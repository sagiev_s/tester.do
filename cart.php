<?php
require "action.php";

$title = "BS | Shop Cart";
require_once "includes/header.php";

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="cart-message"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">Cart Checkout</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2"><b>Product Image</b></div>
                        <div class="col-md-2"><b>Product Title</b></div>
                        <div class="col-md-2"><b>Quantity</b></div>
                        <div class="col-md-2"><b>Product Price</b></div>
                        <div class="col-md-2"><b>Total Price</b></div>
                        <div class="col-md-2"><b>Action #</b></div>
                    </div>
                    <p></p>
                    <hr>
                    <?php foreach ($carts as $cart): ?>
                    <div class="row">
                        <div class="col-md-2"><img src="/images/<?=$cart['product_image'] ?>" alt="" style="max-width: 90px"></div>
                        <div class="col-md-2"><?=$cart['product_title'] ?></div>
                        <div class="col-md-2"><input type="text" class="form-control qty" pid="<?=$cart['id'] ?>" id="qty-<?=$cart['id'] ?>" value="<?=$cart['qty'] ?>"></div>
                        <div class="col-md-2"><input type="text" class="form-control price" pid="<?=$cart['id'] ?>" id="price-<?=$cart['id'] ?>" value="<?=$cart['price'] ?>" disabled></div>
                        <div class="col-md-2"><input type="text" class="form-control total" pid="<?=$cart['id'] ?>" id="total-<?=$cart['id'] ?>" value="<?=$cart['total_amount'] ?>" disabled></div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <a href="#" delete_id = "<?=$cart['product_id'] ?>" class="btn btn-danger delete"><span class="glyphicon glyphicon-trash"></span></a>
                                <a href="#" update_id = "<?=$cart['product_id'] ?>"class="btn btn-primary update"><span class="glyphicon glyphicon-ok-sign"></span></a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php endforeach; ?>
                </div>
                <div class="panel-footer"></div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<?php

include "includes/footer.php";

?>
