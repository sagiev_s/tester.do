<?php

require "includes/config.php";

$categories_q = mysqli_query($connection, "SELECT * FROM `categories` ");
$categories = [];
while($cat = mysqli_fetch_assoc($categories_q)) {
    $categories[] = $cat;
}
if(isset($_POST['category_id']) || isset($_POST['query'])) {
    if(isset($_POST['category_id'])){
        $cid = $_POST['category_id'];
        $queries = mysqli_query($connection, "SELECT * FROM `products` WHERE `category` = '$cid'  ");
    }
    else if(isset($_POST['query'])){
        $query = $_POST['query'];
        $queries = mysqli_query($connection, "SELECT * FROM `products` WHERE `title` OR `desc` LIKE '%$query%'  ");
    }
    if(mysqli_num_rows($queries) > 0){
        $i = 0;
        while ($row = mysqli_fetch_array($queries)) {
            $output .= '
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">'.$row["title"].'</div>
                    <div class="panel-body"><img src="images/'.$row["imgPath"].'" alt=""></div>
                    <div class="panel-heading">$ '.$row["price"].'
                        <button class="btn btn-danger pull-right add-to-cart" pid="'.$row["id"].'">Add to Cart</button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            ';
            $i++;
            if($i%3 == 0){
                $output .='<div class="clearfix"></div>';
            }
        }
    }
    else {
        $output .= '
        <h1>No Product found!</h1>
        ';
    }
    echo $output;
}
if(isset($_POST['pid'])){
    $pid = $_POST['pid'];
    $queries = mysqli_query($connection, "SELECT * FROM `cart` WHERE `product_id` = '$pid'  ");
    if(mysqli_num_rows($queries) > 0){
        echo "Product already added into the cart!";
    }
    else {
        $addQueries = mysqli_query($connection, "SELECT * FROM `products` WHERE `id` = '$pid'  ");
        $row = mysqli_fetch_array($addQueries);
        $id = $row['id'];
        $title = $row['title'];
        $imgPath = $row['imgPath'];
        $price = $row['price'];
        $cartQueries = mysqli_query($connection, "INSERT INTO `cart` (`id`, `product_id`, `ip_add`, `product_title`, `product_image`, `qty`, `price`, `total_amount`) 
        VALUES (NULL, '".$id."', '127', '".$title."', '".$imgPath."', '1', '".$price."', '".$price."')");
        if($cartQueries){
            echo "Product added to Cart!";
        }
    }
}
if(isset($_POST['get_shop_cart'])){
    $queries = mysqli_query($connection, "SELECT * FROM `cart` ");
    $total_amount = 0;
    if(mysqli_num_rows($queries) > 0){
        while ($row = mysqli_fetch_array($queries)) {
            $total_amount += $row['price'];
            echo '
            <div class="row">
                <div class="col-md-4"><img src="images/'.$row["product_image"].'" style="max-width: 60px" alt=""></div>
                <div class="col-md-4">'.$row["product_title"].'</div>
                <div class="col-md-4">$ '.$row["price"].'</div>
            </div>
            ';
        }
        echo '
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="pull-left"><b>Total amount:</b></div>
                <div class="pull-right"><b>$ '.$total_amount.'</b></div>
            </div>
        </div>
        ';

    } else {
        echo '
        <h1>No Product in Cart!</h1>
        ';
    }
}
if(isset($_POST['removeFromCart'])){
    $pid = $_POST['delp'];
    $query = mysqli_query($connection, "DELETE FROM `cart` WHERE `product_id` = '$pid'");
    if($query){
        echo '
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b>Product is removed from cart!</b>
        </div>
        ';
    }else {
        echo '
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b>Opps! Something wrong happen...</b>
        </div>';
    }
}
if(isset($_POST['updateCart'])){
    $pid = $_POST['upid'];
    $qty = $_POST['qty'];
    $total = $_POST['total'];
    $query = mysqli_query($connection, "UPDATE `cart` SET `qty` = '$qty' AND `total_amount` = '$total' WHERE `cart`.`product_id` = '$pid'");
    if($query){
        echo '
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b>'.$qty.'</b>
        </div>
        ';
    }else {
        echo '
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b>Opps! Something wrong happen...</b>
        </div>';
    }
}

$products_q = mysqli_query($connection, "SELECT * FROM `products` ORDER BY RAND() LIMIT 9 ");
$products = [];
while($prod = mysqli_fetch_assoc($products_q)) {
    $products[] = $prod;
}
$cart_q = mysqli_query($connection, "SELECT * FROM `cart` ");
$carts = [];
while($cartl = mysqli_fetch_assoc($cart_q)) {
    $carts[] = $cartl;
}