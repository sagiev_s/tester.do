<?php
require "action.php";
session_start();
include "includes/header.php";

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2">
			<div class="nav nav-pills nav-stacked">
            <?php
            foreach($categories as $cat)
            {
            ?>
				<li><a href="#" class="categories" cid="<?=$cat['id'] ?>"><?=$cat['title'] ?></a></li>
            <?php
            }
            ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-info">
				<div class="panel-heading">Products</div>
				<div class="panel-body" id="product_load">
                    <?php
                    $i = 0;
                    foreach($products as $product) {
                        ?>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading"><?=$product['title'] ?></div>
                                <div class="panel-body"><img src="images/<?=$product['imgPath'] ?>" alt=""></div>
                                <div class="panel-heading">$ <?=$product['price'] ?>
                                    <button class="btn btn-danger pull-right add-to-cart" pid="<?=$product['id'] ?>">Add to Cart</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                        ?>
                        <?php if($i%3 == 0): ?>
                            <div class="clearfix"></div>
                        <?php endif; ?>
                    <?php
                    }
                    ?>
				</div>
				<div class="panel-footer"><?=date('Y') ?></div>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</div>
<?php

include "includes/footer.php";

?>