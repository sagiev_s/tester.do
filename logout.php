<?php

require "action.php";

unset($_SESSION['username']);
unset($_SESSION['logged_in']);

header('location:index.php');