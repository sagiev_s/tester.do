<?php

$connection = new mysqli(
    $config['db']['server'],
    $config['db']['username'],
    $config['db']['password'],
    $config['db']['name']
);
$connection->set_charset("utf8");

if(!$connection){
    die("Connection failed: ". mysqli_connect_error());
}


