<!DOCTYPE html>
<html>
<head>
    <?php
        if(isset($title)){
            echo "<title>$title</title>";
        }
        else {
            echo "<title>Bootstrap Shop</title>";
        }
        ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Bootstrap Shop</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Products</a></li>
                        <form action="" class="navbar-form navbar-left">
                            <div class="form-group">
                                <input type="text" class="form-control" name="search" id="input-search" placeholder="Search">
                            </div>
                            <button type="submit" class="btn btn-default" id="search">Search</button>
                        </form>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#" class="cart">Cart <span class="badge">0</span></a></li>
                        <?php if(isset($_SESSION['logged_in'])  && $_SESSION['logged_in'] == true) : ?>
                        <li><a href="#" class="profile"><?=$_SESSION['username'] ?></a></li>
                        <li><a href="logout.php" class="logout">Logout</a></li>
                        <?php else: ?>
                        <li><a href="#" class="signin">Sign In</a></li>
                        <li><a href="#" class="signup">Sign Up</a></li>
                        <?php endif; ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
                <div class="modal signin-modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Sign in</h4>
                            </div>
                            <form method="post" class="signin-form">
                                <div class="modal-body">
                                    <label for="email">Username:</label>
                                    <input type="text" class="form-control" name="username" id="username" required="">
                                    <label for="password">Password:</label>
                                    <input type="password" class="form-control" name="password" id="password" required="">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="signin-button">Sign In</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="modal signup-modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Sign up</h4>
                            </div>
                            <form method="post" class="signup-form">
                                <div class="modal-body">
                                    <div class="messages"></div>
                                    <label for="email">Username:</label>
                                    <input type="text" class="form-control" name="username" id="username" required="">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" name="email" id="email" required="">
                                    <label for="password">Password:</label>
                                    <input type="password" class="form-control" name="password" id="password" required="">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="signup-button">Sign Up</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="modal cart-modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="row">
                                    <div class="col-md-4">Product image</div>
                                    <div class="col-md-4">Product title</div>
                                    <div class="col-md-4">Product price</div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="product-list"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <a href="cart.php" type="button" class="btn btn-primary">Show more</a>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>