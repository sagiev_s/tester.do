<?php

require "includes/config.php";

$username = $_POST['username'];
$password = $_POST['password'];
$email = $_POST['email'];

$errors = [];
if(trim($username) == ''){
    $errors[] = 'Fill your username!';
}
else if(trim($email) == ''){
    $errors[] = 'Fill your email!';
}
else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $errors[] = 'Invalid email format!';
}
else if(trim($password) == ''){
    $errors[] = 'Fill your password!';
}
else if(mysqli_num_rows(mysqli_query($connection, "SELECT `id` FROM `users` WHERE `email` = '$email'"))) {
    $errors[] = 'This email has been taken!';
}
if(!empty($errors)){
    echo '<div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b>'.array_shift($errors).'</b>
          </div>';
}

else {
    //all's good
    $password = md5($password);
    $created_at = date("Y-m-d H:i:s");
    $updated_at = date("Y-m-d H:i:s");
    $run_query = mysqli_query($connection, "INSERT INTO `users` (`username`, `email`, `password`, `created_at`, `updated_at`) VALUES ('".$username."', '".$email."', '".$password."', '".$created_at."', '".$updated_at."')");
    if($run_query){
        echo '<div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b>You successfully signed up!</b>
          </div>';
    }
}