<?php

mb_internal_encoding("UTF-8");
function mb_ucfirst($text) {
    return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
}

$keywords = $_POST["keyword"];
$titles = $_POST["title"];
$bodies = $_POST["body"];
//$ifdefault = $_POST["ifdefault"];
$type = $_POST["type"];
$arraysK = [];
$arraysK = explode("\n", $keywords);
$arraysT = [];
$arraysT = explode("\n", $titles);
$arraysB = [];
$arraysB = explode("\n", $bodies);
echo '<div class="col-md-2">
      <h4>Ключи</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)){
        $keyword = mb_ucfirst($keyword);
        echo '<p>'.$keyword.'</p>';
    }
}
echo '</div>';
echo '<div class="col-md-4">
      <h4>Заголовок объявления (готовый)</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)) {
        $keyword = mb_ucfirst($keyword);
        //echo $keyword.'<br />';
        foreach ($arraysT as $title) {
            $addTitle = str_replace("{keyword}", $keyword, $title);
            if(($type == "yandex" && (mb_strlen($addTitle)<=33)) || ($type == "google" && (mb_strlen($addTitle)<30))) {
                echo '<p class="bg-default">'.$addTitle . '<span style="color: red">' . mb_strlen($addTitle) . '</span></p>';
            }
            else {
                echo '<p class="bg-danger">'.$addTitle . '<span style="color: red">' . mb_strlen($addTitle) . '</span></p>';
            }
        }
    }
}
echo '</div>';
echo '<div class="col-md-6">
      <h4>Текст объявления (готовый)</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)) {
        $keyword = mb_ucfirst($keyword);
        foreach ($arraysB as $body) {
            $addBody = str_replace("{keyword}", $keyword, $body);
            if(($type == "yandex" && (mb_strlen($addBody)<=75)) || ($type == "google" && (mb_strlen($addBody)<=80))) {
                echo '<p class="bg-default">'.$addBody . '<span style="color: red">' . mb_strlen($addBody) . '</span></p>';
            }
            else {
                echo '<p class="bg-danger">'.$addBody . '<span style="color: red">' . mb_strlen($addBody) . '</span></p>';
            }
        }
    }
}
echo '</div>';

?>