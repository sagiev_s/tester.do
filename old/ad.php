<!DOCTYPE html>
<html>
<head>
    <title>Генератор объявлений AdWords</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">IM</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/direct.php">Генератор объявлений <span class="sr-only">(current)</span></a></li>
                <li><a href="/utm.php">Генератор UTM-меток</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row">
        <h1>Генератор объявлений AdWords</h1>
        <p class="bg-danger">Задайте ключевые фразы, маски заголовков и текстов объявлений. Система сама выбирает подходящую маску для каждой ключевой фразы, которая ближе к максимально допустимой длине заголовка или текста</p>
        <p>Каждую фразу или шаблон нужно разделять через новую строку. Максимальная длина заголовков и текста объявлении:</p>
        <div class="col-md-4">
            <label for="template">Ключевые фразы:</label><br>
            <textarea name="keyword" id="keyword" cols="6" rows="10" class="form-control" placeholder="Каждый запрос с новой строки" required="">купи слона
купить слона в москве
купи слона магазин
купить слона интернет магазин недорого
где купить слона
купить слона из дерева
купить индийского слона
купить слона живого
купить розового слона
слон большой купить
белый слон купить</textarea>
        </div>
        <div class="col-md-4">
            <label for="url">URL сайта:</label>
            <input type="text" name="url" class="form-control" id="url">
        </div>
        <div class="col-md-2">
            <label for="suburl">/</label>
            <input type="text" name="suburl" class="form-control" id="suburl">
        </div>
        <div class="col-md-2">
            <label for="suburl2">/</label>
            <input type="text" name="suburl2" class="form-control" id="suburl2">
        </div>
    </div>
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-md-6">
            <label for="template1">Шаблоны заголовков #1(*30):</label><br>
            <textarea name="title1" id="title1" cols="6"  rows="4" class="form-control" required="">{keyword}. Гарантия 10 лет. Звоните!
{keyword}. Без пробега. Звоните!</textarea>
            <label for="template2">Шаблоны заголовков #2(*30):</label><br>
            <textarea name="title2" id="title2" cols="6"  rows="4" class="form-control" required="">
{keyword}. Скидки. Звоните!</textarea>
        </div>
        <div class="col-md-6">
            <label for="template">Шаблоны текстов(*80):</label><br>
            <textarea name="body" id="body" cols="6" rows="6" class="form-control" required="">Самосвал в подарок! {keyword}. Гарантия 10 лет. Звоните!
Самосвал в подарок! {keyword}. Без пробега. Звоните!
Самосвал в подарок! {keyword}. Скидки. Звоните!</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-info" style="margin-top: 10px">Старт</button>
            <p></p>
            <p></p>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="show"></div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
</div>
<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
    $(".btn").click(function() {
        if ($('#keyword').val() && $('#title1').val() && $('#body').val() != '') {
            if ($("#title1").val().indexOf("{keyword}") !== -1 && $("#body").val().indexOf("{keyword}") !== -1) {
                // contains
                var data = { keyword: $("#keyword").val(),
                    title1: $("#title1").val(),
                    title2: $("#title2").val(),
                    body: $("#body").val(),
                    url: $("#url").val(),
                    suburl: $("#suburl").val(),
                    suburl2: $("#suburl2").val(),
                }
                $.ajax({
                    method: 'POST',
                    data: data,
                    url: 'back-ad.php',
                    success: function(data) {
                        $(".show").html(data);
                    },
                });
            } else {
                alert('Добавьте {keyword}!');
            }
        } else {
            alert('Не все поля заполнены!');
        }
    });
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>