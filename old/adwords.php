<!DOCTYPE html>
<html>
<head>
	<title>Генератор объявлений</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<div class="row">
		<h1>Генератор объявлений</h1>
		<p class="bg-danger">Задайте ключевые фразы, маски заголовков и текстов объявлений. Система сама выбирает подходящую маску для каждой ключевой фразы, которая ближе к максимально допустимой длине заголовка или текста</p>
		<p>Каждую фразу или шаблон нужно разделять через новую строку. Для подстановки нужно использовать {keyword}</p>
		<div class="row">
				<form action="back.php" method='post'>
				<div class="row">
					<div class="col-md-4">
	        	<div class="form-group">
	            <label for="keyword">Ключевые фразы:</label>
	            <textarea name="keyword" rows="11" class="form-control">купи слона
купить слона в москве
купи слона магазин</textarea>
	        	</div>
	        </div>
	        <div class="col-md-4">
	        	<div class="form-group">
	            <label for="url">URL сайта:</label>
	            <input type="text" name="url" class="form-control" value="http://i-marketing.kz">
	        	</div>
	        </div>
	        <div class="col-md-2">
	            <label for="suburl">/</label>
	            <input type="text" name="suburl" class="form-control" value="продвижение">
	        </div>
	        <div class="col-md-2">
	            <label for="suburl2">/</label>
	            <input type="text" name="suburl2" class="form-control" value="сайтов">
	        </div>
        </div>
				<div class="col-md-4">
	        <div class="form-group">
	            <label for="hd1">Залоговок 1</label>
	            <textarea name="hd1" rows="11" class="form-control">{keyword}. Компрессор
{keyword}. Компрессоры в Казахстане
{keyword}. Компрессор в Алматы</textarea>
	        	</div>
	        </div>
	      <div class="col-md-4">
	        <div class="form-group">
	            <label for="hd2">Залоговок 2</label>
	            <textarea name="hd2" rows="11" class="form-control">Собственного производства
Качественного производства
Собственного производства
Собственного производства
Собственного производства
Собственного производства
От Собек-сервиса
От Собек-сервиса
От Собек-сервиса
Цены от производителя
Собственного производства
Цены от производителя
Качественного производства
Цены от производителя
От Собек-сервиса
От Собек-сервиса
Качественного производства
Качественного производства
Собственного производства
От Собек-сервиса
Цены от производителя
Качественного производства
Качественного производства
Качественного производства</textarea>
	        	</div>
					</div>
					<div class="col-md-4">
	        	<div class="form-group">
	            <label for="desc">Описание</label>
	            <textarea name="desc" rows="11" class="form-control">Предоставляем гарантию и оперативный сервис.Качественные компрессоры от Собек.
Предоставляем гарантию и оперативный сервис.Качественные компрессоры от Собек.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Предоставляем гарантию и оперативный сервис.Качественные компрессоры от Собек.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Поршневой компрессор с ременным приводом. Ресивер 200 л. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Лучший производитель поршневых компрессоров в Казахстане. Качество гарантируем.
Предоставляем гарантию и оперативный сервис.Качественные компрессоры от Собек.
Предоставляем гарантию и оперативный сервис.Качественные компрессоры от Собек.
Предоставляем гарантию и оперативный сервис.Качественные компрессоры от Собек.</textarea>
	        	</div>
	        </div>
	        <button type="submit" class="btn btn-primary">Submit</button>
	      </form>
			</div>
		</div>
	</div>
</div>
<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>