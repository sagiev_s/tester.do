<?php 

	// $keywords = $_POST["keyword"];
	// $titles = $_POST["title"];
	// $bodies = $_POST["body"];
	// $arraysK = [];
	// $arraysK = explode("\n", $keywords);
	// $arraysT = [];
	// $arraysT = explode("\n", $titles);
	// $arraysB = [];
	// $arraysB = explode("\n", $bodies);
	// foreach ($arraysK as $keyword) {
	// 	echo $keyword.'<br />';
	// }

$hd1 = $_POST["hd1"];
$hd1_arr =[];
$hd1_arr = explode("\n", $hd1);
$hd2 = $_POST["hd2"];
$hd2_arr =[];
$hd2_arr = explode("\n", $hd2);
$desc = $_POST["desc"];
$desc_arr =[];
$desc_arr = explode("\n", $desc);

$keywords = $_POST["keyword"];
$url = $_POST["url"];
$suburl = $_POST["suburl"];
$suburl2 = $_POST["suburl2"];
$keywords_arr = [];
$keywords_arr = explode("\n", $keywords);

$hd1_ar = [];
$hd2_ar = [];
$desc_ar = [];

require_once('PHPExcel.php');
// Подключаем класс для вывода данных в формате excel
require_once('PHPExcel/Writer/Excel5.php');

// Создаем объект класса PHPExcel
$xls = new PHPExcel();
// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
// Подписываем лист
$sheet->setTitle('Объявления');

$sheet->getStyle('A1')->getFont()->setBold(true);
$sheet->getStyle('B1')->getFont()->setBold(true);
$sheet->getStyle('C1')->getFont()->setBold(true);
$sheet->getStyle('D1')->getFont()->setBold(true);
$sheet->getStyle('E1')->getFont()->setBold(true);
$sheet->getStyle('F1')->getFont()->setBold(true);
$sheet->getStyle('G1')->getFont()->setBold(true);

// Заголовок 1
if(preg_match('/keyword/', $hd1)) { 
	foreach ($keywords_arr as $keyword) {
	foreach ($hd1_arr as $title) {
    $addTitle1 = str_replace("{keyword}", $keyword, $title);
    array_push($hd1_ar, $addTitle1);
  }
}

$cnt = count($hd1_ar);
for ($i=0; $i <= $cnt; $i++) { 
	  $sheet->setCellValueByColumnAndRow(
                                    0,
                                    $i+1,
                                    $hd1_ar[$i-1]);
	  $sheet->setCellValueByColumnAndRow(
                                1,
                                $i+1,
                                strlen($hd1_ar[$i-1]));
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(0, $i+1)->getAlignment()->
		          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
}
} 
else {
$cnt = count($hd1_arr);
for ($i=0; $i <= $cnt; $i++) { 
	  $sheet->setCellValueByColumnAndRow(
                                    0,
                                    $i+1,
                                    $hd1_arr[$i-1]);
	  $sheet->setCellValueByColumnAndRow(
                                1,
                                $i+1,
                                strlen($hd1_arr[$i-1]));
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(0, $i+1)->getAlignment()->
		          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
}
} 
// Заголовок 2
if(preg_match('/keyword/', $hd2)) { 
	foreach ($keywords_arr as $keyword) {
	foreach ($hd2_arr as $title) {
    $addTitle2 = str_replace("{keyword}", $keyword, $title);
    array_push($hd2_ar, $addTitle2);
  }
}

$cnt2 = count($hd2_ar);
for ($i=0; $i <= $cnt2; $i++) { 
	  $sheet->setCellValueByColumnAndRow(
                                    2,
                                    $i+1,
                                    $hd2_ar[$i-1]);
	  $sheet->setCellValueByColumnAndRow(
                                    3,
                                    $i+1,
                                    strlen($hd2_ar[$i-1]));
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(2, $i+1)->getAlignment()->
		          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
}
}
else {
	$cnt2 = count($hd2_arr);
	for ($i=0; $i <= $cnt2; $i++) { 
		  $sheet->setCellValueByColumnAndRow(
	                                    2,
	                                    $i+1,
	                                    $hd2_arr[$i-1]);
		  $sheet->setCellValueByColumnAndRow(
                                    3,
                                    $i+1,
                                    strlen($hd2_arr[$i-1]));
			// Применяем выравнивание
			$sheet->getStyleByColumnAndRow(2, $i+1)->getAlignment()->
			          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
}
// Описание
if(preg_match('/keyword/', $desc)) { 
	foreach ($keywords_arr as $keyword) {
	foreach ($desc_arr as $title) {
    $addDesc = str_replace("{keyword}", $keyword, $title);
    array_push($hd2_ar, $addDesc);
  }
}

$cnt3 = count($desc_ar);
for ($i=0; $i <= $cnt3; $i++) { 
	  $sheet->setCellValueByColumnAndRow(
                                    4,
                                    $i+1,
                                    $desc_ar[$i-1]);
	  $sheet->setCellValueByColumnAndRow(
                                    5,
                                    $i+1,
                                    strlen($desc_ar[$i-1]));
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(4, $i+1)->getAlignment()->
		          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
}
}
else {
	$cnt3 = count($desc_arr);
	for ($i=0; $i <= $cnt3; $i++) { 
		  $sheet->setCellValueByColumnAndRow(
	                                    4,
	                                    $i+1,
	                                    $desc_arr[$i-1]);
		  	  $sheet->setCellValueByColumnAndRow(
                                    5,
                                    $i+1,
                                    strlen($desc_arr[$i-1]));
			// Применяем выравнивание
			$sheet->getStyleByColumnAndRow(4, $i+1)->getAlignment()->
			          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
}
//URL
if(preg_match('/http/', $url)) { 
	for ($i=0; $i <= $cnt; $i++) { 
		  $sheet->setCellValueByColumnAndRow(
	                                    6,
	                                    $i+1,
	                                    $url.'/'.$suburl.'/'.$suburl2);
			// Применяем выравнивание
			$sheet->getStyleByColumnAndRow(6, $i+1)->getAlignment()->
			          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
}
else {
	for ($i=0; $i <= $cnt; $i++) { 
		  $sheet->setCellValueByColumnAndRow(
	                                    6,
	                                    $i+1,
	                                    'http://'.$url.'/'.$suburl.'/'.$suburl2);
			// Применяем выравнивание
			$sheet->getStyleByColumnAndRow(6, $i+1)->getAlignment()->
			          setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
}


$sheet->setCellValue("A1", 'Headline 1');
$sheet->setCellValue("B1", '30');
$sheet->setCellValue("C1", 'Headline 2');
$sheet->setCellValue("D1", '30');
$sheet->setCellValue("E1", 'Description');
$sheet->setCellValue("F1", '80');
$sheet->setCellValue("G1", 'Destination URL');


 header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
 header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
 header ( "Cache-Control: no-cache, must-revalidate" );
 header ( "Pragma: no-cache" );
 header ( "Content-type: application/vnd.ms-excel" );
 header ( "Content-Disposition: attachment; filename=matrix.xls" );

// Выводим содержимое файла
 $objWriter = new PHPExcel_Writer_Excel5($xls);
 $objWriter->save('php://output');

?>