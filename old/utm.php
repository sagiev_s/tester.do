<!DOCTYPE html>
<html>
<head>
    <title>IM | Генератор UTM-меток</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<style>
    .radio {
        display: inline-block;
        vertical-align: middle;
        padding-left: 20px;
        min-height: 20px;
        padding-top: 5px;
        margin-bottom: 0;
        line-height: 20px;
    }
</style>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">IM</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/direct.php">Генератор объявлений <span class="sr-only">(current)</span></a></li>
                <li><a href="/utm.php">Генератор UTM-меток</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12 bg-info">
            <h1>Генератор UTM-меток</h1>
            <label for="url">Адрес страницы:</label><br>
            <input type="text" class="form-control" name="url" id="url" placeholder="google.com">
<!--            <label for="choose">Адрес страницы:</label><br>-->
            <legend>Выберите источник трафика</legend>
            <div class="row">
                <label class="radio inline">
                    <input type="radio" name="source" id="other" value="other" checked> Произвольно
                </label>
                <label class="radio inline">
                    <input type="radio" name="source" id="adwords" value="adwords">
                    Google Adwords
                </label>
                <label class="radio inline">
                    <input type="radio" name="source" id="direct" value="direct">
                    Яндекс.Директ
                </label>
                <label class="radio inline">
                    <input type="radio" name="source" id="vk" value="vk">
                    Вконтакте
                </label>
            </div>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="utm_source">Источник кампании</label><br>
            <input type="text" class="form-control" name="utm_source" id="utm_source" placeholder="google, yandex, vk...">

        </div>
        <div class="col-md-6">
            <label for="utm_medium">Тип трафика</label><br>
            <input type="text" class="form-control" name="utm_medium" id="utm_medium" placeholder="cpc, email, banner">
        </div>
        <div class="col-md-6">
            <label for="utm_campaign">Название кампании</label><br>
            <input type="text" class="form-control" name="utm_campaign" id="utm_campaign" placeholder="К примеру: google-poisk">
        </div>
        <div class="col-md-6">
            <label for="utm_content">Идентификатор объявления</label><br>
            <input type="text" class="form-control" name="utm_content" id="utm_content" placeholder="К примеру: banner-full-2">
        </div>
        <div class="col-md-6">
            <label for="utm_term">Ключевое слово</label><br>
            <input type="text" class="form-control" name="utm_term" id="utm_term" placeholder="К примеру: купить iPhone">
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p></p>
            <input type="submit" class="btn btn-primary btn-block" value="Сгенерировать ссылку">
            <br>
            <br>
            <div class="result alert alert-success" style="display: none"></div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
    $(document).ready(function (){
        $(".btn").click(function() {
            if ($('#url').val() && $('#utm_source').val() && $('#utm_medium').val() != '') {
                var data = {
                    url: $("#url").val(),
                    utm_source: $("#utm_source").val(),
                    utm_medium: $("#utm_medium").val(),
                    utm_campaign: $("#utm_campaign").val(),
                    utm_content: $("#utm_content").val(),
                    utm_term: $("#utm_term").val(),
                }
                $(".result").show();
                $.ajax({
                    method: 'POST',
                    data: data,
                    url: 'back-utm.php',
                    success: function (data) {
                        $(".result").html(data);
                    },
                });
            }
            else {
                alert("Не все поля заполнены!")
            }
        });

        $('#adwords').change(function() {
            $('#utm_source').val("google");
            $('#utm_campaign').val("{campaignid}");
            $('#utm_content').val("{creative}");
            $('#utm_term').val("{keyword}");
        })
        $('#direct').change(function() {
            $('#utm_source').val("yandex");
            $('#utm_campaign').val("{campaign_id}");
            $('#utm_content').val("{ad_id}");
            $('#utm_term').val("{keyword}");
        })
        $('#vk').change(function() {
            $res = $(this).val();
            $('#utm_source').val($res);
            $('#utm_campaign').val("{campaign_id}");
            $('#utm_content').val("{ad_id}");
            $('#utm_term').val("");
        })
        $('#other').change(function() {
            $('#utm_source').val("");
            $('#utm_medium').val("");
            $('#utm_campaign').val("");
            $('#utm_content').val("");
            $('#utm_term').val("");
        })
        $('input[name=source]').change(function() {
            $('#utm_medium').val("cpc");
        })
    });
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>