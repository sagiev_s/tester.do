<?php

mb_internal_encoding("UTF-8");
function mb_ucfirst($text) {
    return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
}

$keywords = $_POST["keyword"];
$titles1 = $_POST["title1"];
$titles2 = $_POST["title2"];
$bodies = $_POST["body"];
$url = $_POST["url"];
$suburl = $_POST["suburl"];
$suburl2 = $_POST["suburl2"];
$arraysK = [];
$arraysK = explode("\n", $keywords);
$arraysT1 = [];
$arraysT1 = explode("\n", $titles1);
$arraysT2 = [];
$arraysT2 = explode("\n", $titles2);
$arraysB = [];
$arraysB = explode("\n", $bodies);
echo '<div class="col-md-2">
      <h4>Ключи</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)){
        $keyword = mb_ucfirst($keyword);
        echo '<p>'.$keyword.'</p>';
    }
}
echo '</div>';
echo '<div class="col-md-2">
      <h4>Заголовок объявления #1</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)) {
        $keyword = mb_ucfirst($keyword);
        //echo $keyword.'<br />';
        foreach ($arraysT1 as $title) {
            $addTitle1 = str_replace("{keyword}", $keyword, $title);
            if(mb_strlen($addTitle1)<=30) {
                echo '<p class="bg-default">'.$addTitle1 . '</p>';
            }
            else {
                echo '<p class="bg-danger">'.$addTitle1 . '</p>';
            }
        }
    }
}
echo '</div>';
echo '<div class="col-md-2">
      <h4>Заголовок объявления  #2</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)) {
        $keyword = mb_ucfirst($keyword);
        //echo $keyword.'<br />';
        foreach ($arraysT2 as $title) {
            $addTitle2 = str_replace("{keyword}", $keyword, $title);
            if(mb_strlen($addTitle2)<=30) {
                echo '<p class="bg-default">'.$addTitle2 . '</p>';
            }
            else {
                echo '<p class="bg-danger">'.$addTitle2 . '</p>';
            }
        }
    }
}
echo '</div>';
echo '<div class="col-md-4">
      <h4>Текст объявления (готовый)</h4>';
foreach ($arraysK as $keyword) {
    if(!empty($keyword)) {
        $keyword = mb_ucfirst($keyword);
        foreach ($arraysB as $body) {
            $addBody = str_replace("{keyword}", $keyword, $body);
            if(mb_strlen($addBody)<=80) {
                echo '<p class="bg-default">'.$addBody . '</p>';
            }
            else {
                echo '<p class="bg-danger">'.$addBody . '</p>';
            }
        }
    }
}
echo '</div>';
echo '<div class="col-md-2">
      <h4>URL объявления</h4>
      <p class="bg-success">www.'.$url.'/'.$suburl.'/'.$suburl2.'</p>
      </div>';

?>